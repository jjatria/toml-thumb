use TOML::Thumb;
use Test;

my $todo = set <
    array/tables-1
    inline-table/linebreak-1
    inline-table/linebreak-2
    inline-table/linebreak-3
    inline-table/linebreak-4
    key/after-array
    key/after-table
    key/after-value
    key/no-eol
    table/duplicate
>;

for sort 't/invalid'.IO.map: { .d ?? |.dir».&?BLOCK !! $_ } {
    my $name = .extension('').subst: /^ 't/invalid/' /;

    todo 'not yet implemented' if $todo{$name};

    is-deeply try { .&from-toml }, Nil, $name;
}

done-testing;
