unit module TOML::Thumb:ver<0.2>;

class Date::Local     is Date     {}
class DateTime::Local is DateTime { method Str { callsame.subst: 'Z' } }

# FIXME Raku needs a Time.
class Time::Local does Dateish {
    has $.hour;
    has $.min;
    has $.sec;

    method !formatter() {
        sprintf '%02d:%02d:%s', $!hour, $!min, $!sec.floor == $!sec
            ?? $!sec.Int.fmt: '%02d' !! $!sec.fmt: '%09.6f';
    }

    multi method new($hour, $min, $sec) { self.bless(:$hour :$min :$sec) }

    multi method new(Str $time) {
        $?CLASS.new(|($time ~~ /(\d\d) ':' (\d\d) ':' (\d\d[\. \d+]?)/));
    }
}

grammar Grammar {
    # Walk each key part into the structure. Return a pointer to there.
    sub walk-key($ptr is rw, $cap) {
        for $cap<key> {
            $ptr := $ptr.tail if $ptr ~~ Array;

            $ptr := $ptr{ .made };
        }

        return-rw $ptr;
    }

    rule TOP { <.ws> [ <array-table> | <kv> | <table> ]* {
        my $data = {};

        for $/.caps -> (:key($type), :value($cap)) {
            my $ptr := walk-key($data, $cap);

            given $type {
                when 'kv' {
                    # TODO Give better, positional errors.
                    die 'Key already in use' if $ptr.defined;

                    $ptr = $cap<value>.made;
                }
                when 'table' {
                    $ptr //= {};

                    # TODO Give better, positional errors.
                    die 'Key already in use' if $ptr !~~ Hash;

                    walk-key($ptr, $_) = .<value>.made for $cap<kv>;
                }
                when 'array-table' {
                    $ptr //= [];

                    # TODO Give better, positional errors.
                    die 'Key already in use' if $ptr !~~ Array;

                    $ptr := $ptr.push({}).tail;

                    walk-key($ptr, $_) = .<value>.made for $cap<kv>;
                }
            }
        }

        make $data;
    } }

    # Whitespace means tab (0x09) or space (0x20).
    token sp { <[\ \t]>* }

    # Control characters other than tab are not permitted in comments.
    token ws { [ \s | '#' <-:Cc +[\t]>* ]* }

    token key-sep { <.sp> '.' <.sp> }

    token kv { <key>+ % <.key-sep> <.sp> '=' <.sp> <value> <.ws> }

    rule       table {  '[' ~ ']'  <key>+ % <.key-sep> <kv>* }
    rule array-table { '[[' ~ ']]' <key>+ % <.key-sep> <kv>* }

    token key {
        |   <basic-str>             { make   $<basic-str>.made }
        | <literal-str>             { make $<literal-str>.made }
        | <[ A..Z a..z 0..9 _ - ]>+ { make ~$/ }
    }

    token value {
        | <[+-]>? inf    { make $/.starts-with("-") ?? -Inf !! Inf }
        | <[+-]>? nan    { make NaN }
        | <inline-array> { make $<inline-array>.made }
        | <inline-table> { make $<inline-table>.made }
        | false          { make False }
        | true           { make True  }

        # Date/Time
        | <.date>                  { make Date::Local.new: ~$/ }
        |                  <.time> { make Time::Local.new: ~$/ }
        | <.date> <[Tt\ ]> <.time> <offset>? {
            my $class = $<offset>:exists ?? DateTime !! DateTime::Local;

            # Raku only supports ISO 8601, not RFC 3339, so replace the space.
            make $class.new: $/.trans: ' ' => 'T';
        }

        # Integer/Float
        | <[+-]>?                           # Sign
        [ 0 | <[1..9]> [ _? [\d+]+ % _ ]? ] # Integer
        [ '.'               [\d+]+ % _ ]?   # Fraction
        [ <[Ee]> <[+-]>?    [\d+]+ % _ ]?   # Exponent
        { make +$/ }

        # Prefixed Integer
        | '0x' [ <xdigit>+ ]+ % _ { make +$/ } # Hexadecimal
        | '0o' [ <[0..7]>+ ]+ % _ { make +$/ } # Octal
        | '0b' [ <[0..1]>+ ]+ % _ { make +$/ } # Binary

        # String
        |   <basic-str> { make   $<basic-str>.made }
        | <literal-str> { make $<literal-str>.made }

        # Multi-line String
        | "'''" \n? <( <-:Cc +[\t\n]>*? )>       "'''" { make ~$/ }
        | '"""' \n? [ <basic-chr> | <newline> ]* '"""' { make $/.caps».value».made.join }
    }

    # Date/Time
    token date   { \d**4 '-' \d\d '-' \d\d }
    token time   { \d**2 ':' \d\d ':' \d\d [ '.' \d+ ]? }
    token offset { <[Zz]> | <[+-]> \d+ ':' \d+ }

    # Inline Array/Table. Array allows trailing commas.
    rule inline-array { '[' ~ ']' <value>* %% ',' { make $<value>».made   } }
    rule inline-table { '{' ~ '}' <kv>*    %  ',' {
        make $<kv>.map({ .<key>[0].made => .<value>.made }).Hash;
    } }

    # String
    token   basic-str { '"' ~ '"' <basic-chr>*   { make $<basic-chr>».made.join } }
    token literal-str { "'" ~ "'" (<-:Cc -[']>*) { make ~$0 } }

    token newline { "\n" { make ~$/ } }
    token basic-chr {
        | <-:Cc -[ " \\ ]>+     { make ~$/ } # "
        | ｢\b｣                  { make "\b" }
        | ｢\t｣                  { make "\t" }
        | ｢\n｣                  { make "\n" }
        | ｢\f｣                  { make "\f" }
        | ｢\r｣                  { make "\r" }
        | ｢\"｣                  { make "\"" }
        | ｢\\｣                  { make "\\" }
        | ｢\u｣ (<.xdigit> ** 4) { make chr :16(~$0) }
        | ｢\U｣ (<.xdigit> ** 8) { make chr :16(~$0) }
    }
}

multi sub from-toml(IO  $toml) is export { Grammar.parse($toml.slurp).made }
multi sub from-toml(Str $toml) is export { Grammar.parse($toml      ).made }

# Quote complicated keys. TODO Share this regex with Grammar.bare-key somehow.
sub key { $^key ~~ /^ <[ A..Z a..z 0..9 _ - ]>+ $/ ?? $key !! toml($key) }

# FIXME Assumes we have a hash of hashes.
sub to-toml(%hash) is export {
    my $toml = '';

    my $all-hashes = %hash.values.all ~~ Hash;

    for %hash.sort: *.key.fc {
        if $all-hashes {
            my $max = .value.keys».chars.max;

            # Sort keys alphabetically but put single line values first.
            $toml ~= '[' ~ key(.key) ~ "]\n";
            $toml ~= sprintf "%-*s = %s\n", $max, key(.key), toml(.value)
                for .value.sort: { .value ~~ Str && .value.contains("\n"), .key.fc };
            $toml ~= "\n";
        }
        else {
            $toml ~= key(.key) ~ ' = ' ~ toml(.value) ~ "\n";
        }
    }

    $toml.chomp;
}

multi sub toml(Bool     $value) { $value.lc }
multi sub toml(Dateish  $value) { ~$value }
multi sub toml(Hash     $value) {
    '{ ' ~ $value.map({ key(.key) ~ ' = ' ~ toml(.value) }).join(", ") ~ ' }';
}
multi sub toml(Iterable $value) { '[ ' ~ $value.map(*.&toml).join(", ") ~ ' ]' }
multi sub toml(Numeric  $value) { $value.lc }
multi sub toml(Any      $value is copy ) {
    return qq{'''\n$value'''} if $value.contains("\n");
    return qq{'$value'} if $value ~~ /^ <+[\N] -[\x00..\x1F] -['] -[\x7F]>* $/;

    $value ~~ s:g/ $<x> = <[ \x00..\x1F \x7F ]> /{ $<x>.encode.head.fmt: Q'\u%04x' }/;
    $value ~~ s:g/\"/'\\\"/;

    return '"' ~ $value ~ '"'
}
